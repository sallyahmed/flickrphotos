//
//  NSString+General.h
//  FlickrPhotos
//
//  Created by Sally on 5/24/16.
//  Copyright © 2016 Sally Ahmed. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (General)
- (BOOL)isEmpty;

- (NSString *)urlencode;
@end

#import <UIKit/UIKit.h>

@interface FlickrPhoto : NSObject

@property (nonatomic, strong) NSString * farm;
@property (nonatomic, strong) NSString * idField;
@property (nonatomic, strong) NSString * isfamily;
@property (nonatomic, strong) NSString * isfriend;
@property (nonatomic, strong) NSString * ispublic;
@property (nonatomic, strong) NSString * owner;
@property (nonatomic, strong) NSString * secret;
@property (nonatomic, strong) NSString * server;
@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSString * imgURL;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end
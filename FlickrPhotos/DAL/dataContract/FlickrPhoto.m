//
//	FlickrPhoto.m
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport



#import "FlickrPhoto.h"

@interface FlickrPhoto ()
@end
@implementation FlickrPhoto




/**
 * Instantiate the instance using the passed dictionary values to set the properties values
 */

-(instancetype)initWithDictionary:(NSDictionary *)dictionary
{
	self = [super init];
	if(![dictionary[@"farm"] isKindOfClass:[NSNull class]]){
		self.farm = dictionary[@"farm"];
	}	
	if(![dictionary[@"id"] isKindOfClass:[NSNull class]]){
		self.idField = dictionary[@"id"];
	}	
	if(![dictionary[@"isfamily"] isKindOfClass:[NSNull class]]){
		self.isfamily = dictionary[@"isfamily"];
	}	
	if(![dictionary[@"isfriend"] isKindOfClass:[NSNull class]]){
		self.isfriend = dictionary[@"isfriend"];
	}	
	if(![dictionary[@"ispublic"] isKindOfClass:[NSNull class]]){
		self.ispublic = dictionary[@"ispublic"];
	}	
	if(![dictionary[@"owner"] isKindOfClass:[NSNull class]]){
		self.owner = dictionary[@"owner"];
	}	
	if(![dictionary[@"secret"] isKindOfClass:[NSNull class]]){
		self.secret = dictionary[@"secret"];
	}	
	if(![dictionary[@"server"] isKindOfClass:[NSNull class]]){
		self.server = dictionary[@"server"];
	}	
	if(![dictionary[@"title"] isKindOfClass:[NSNull class]]){
		self.title = dictionary[@"title"];
	}
    else{
        self.title = @"";
    }
    
    self.imgURL =  [NSString stringWithFormat:@"http://farm%@.static.flickr.com/%@/%@_%@_m.jpg",
                    self.farm,self.server,
                    self.idField, self.secret];
	return self;
}


/**
 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
 */
-(NSDictionary *)toDictionary
{
	NSMutableDictionary * dictionary = [NSMutableDictionary dictionary];
	if(self.farm != nil){
		dictionary[@"farm"] = self.farm;
	}
	if(self.idField != nil){
		dictionary[@"id"] = self.idField;
	}
	if(self.isfamily != nil){
		dictionary[@"isfamily"] = self.isfamily;
	}
	if(self.isfriend != nil){
		dictionary[@"isfriend"] = self.isfriend;
	}
	if(self.ispublic != nil){
		dictionary[@"ispublic"] = self.ispublic;
	}
	if(self.owner != nil){
		dictionary[@"owner"] = self.owner;
	}
	if(self.secret != nil){
		dictionary[@"secret"] = self.secret;
	}
	if(self.server != nil){
		dictionary[@"server"] = self.server;
	}
	if(self.title != nil){
		dictionary[@"title"] = self.title;
	}
	return dictionary;

}

/**
 * Implementation of NSCoding encoding method
 */
/**
 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
 */
- (void)encodeWithCoder:(NSCoder *)aCoder
{
	if(self.farm != nil){
		[aCoder encodeObject:self.farm forKey:@"farm"];
	}
	if(self.idField != nil){
		[aCoder encodeObject:self.idField forKey:@"id"];
	}
	if(self.isfamily != nil){
		[aCoder encodeObject:self.isfamily forKey:@"isfamily"];
	}
	if(self.isfriend != nil){
		[aCoder encodeObject:self.isfriend forKey:@"isfriend"];
	}
	if(self.ispublic != nil){
		[aCoder encodeObject:self.ispublic forKey:@"ispublic"];
	}
	if(self.owner != nil){
		[aCoder encodeObject:self.owner forKey:@"owner"];
	}
	if(self.secret != nil){
		[aCoder encodeObject:self.secret forKey:@"secret"];
	}
	if(self.server != nil){
		[aCoder encodeObject:self.server forKey:@"server"];
	}
	if(self.title != nil){
		[aCoder encodeObject:self.title forKey:@"title"];
	}

}

/**
 * Implementation of NSCoding initWithCoder: method
 */
- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
	self = [super init];
	self.farm = [aDecoder decodeObjectForKey:@"farm"];
	self.idField = [aDecoder decodeObjectForKey:@"id"];
	self.isfamily = [aDecoder decodeObjectForKey:@"isfamily"];
	self.isfriend = [aDecoder decodeObjectForKey:@"isfriend"];
	self.ispublic = [aDecoder decodeObjectForKey:@"ispublic"];
	self.owner = [aDecoder decodeObjectForKey:@"owner"];
	self.secret = [aDecoder decodeObjectForKey:@"secret"];
	self.server = [aDecoder decodeObjectForKey:@"server"];
	self.title = [aDecoder decodeObjectForKey:@"title"];
	return self;

}
@end
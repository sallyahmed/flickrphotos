 //
//  ServerManager.m
//  AlAmoudi
//
//  Created by Tech World Company on 9/2/15.
//  Copyright (c) 2015 SallyZone. All rights reserved.
//

#import "ServerManager.h"

@implementation ServerManager
Reachability *reach;
NSURLConnection *connection;
NSMutableData*receivedData;


NSURL *url;
NSMutableURLRequest *request;
id returnValue;
int statusCode;
BOOL isparseError= NO;
BOOL isJSON = NO;

NSMutableDictionary * json;
-(id)initWithDelegate:(id <ServerManagerDelegate>)delegate{
    self = [super init];
    if (self) {
        self.delegate = delegate;
    }
    return self;
}


-(void)webConnectionWithFunction:(NSString *) method {
    [self httpConnectUrl:[NSString stringWithFormat:@"%@%@",BASE_URL,method] andParamsData:nil];
}
#pragma  mark - Json WebService & httpConnectUrl -

-(void)httpConnectUrl :(NSString*)urlString andParamsData:(NSMutableDictionary*)params {
    [connection cancel];
    NSError * error;
    returnValue = nil;
      
    if([ServerManager checkInternetConnection] != 0){
//        NSData* data = [NSJSONSerialization dataWithJSONObject:params options:kNilOptions error:&error];

        
      //  NSMutableData *body = [NSMutableData data];

        
        NSURL *url =[NSURL URLWithString:urlString];
        request = [NSMutableURLRequest requestWithURL:url ];
//        NSString *strLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
//
//        
//        [request addValue:strLength forHTTPHeaderField:@"Content-Length"];
        [request setHTTPMethod:@"GET"];
//        [request setHTTPBody: body];
        //Solve ERROR: unable to get the receiver data from the DB!
        [request setCachePolicy:NSURLRequestReturnCacheDataElseLoad];
        receivedData = [[NSMutableData alloc] init];
        
        connection = [[NSURLConnection alloc] initWithRequest:request
                                                     delegate:self
                                             startImmediately:NO];
        [connection setDelegateQueue:[NSOperationQueue mainQueue]];
        [connection start];
        
        
        
        
        
    }
    else{
        
        error = [NSError errorWithDomain:AMLocalizedString(@"NO_INTERNT_CONNECTION", nil) code:4 userInfo:nil];
        
        
        [self.delegate serverConnection:self ISFailedByReason:error];
        
        
    }
    
}




-(void)jsonDataLoad :(NSData *)jsonData{
    
    if ([receivedData length] == 0 ) {
        NSError*  error = [NSError errorWithDomain:AMLocalizedString(@"NO_DATA_RECIVED", nil) code:0
                                          userInfo:nil];
        
        [self.delegate serverConnection:self ISFailedByReason:error];
        return;
    }

    NSString* newStr = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
  //  NSLog(@"result %@ " , newStr);
    NSString* dic = [[newStr componentsSeparatedByString:@"The table 'system_log' is full"]objectAtIndex:0];
    NSData *data = [dic dataUsingEncoding:NSUTF8StringEncoding];
 
    NSMutableDictionary* result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    
    
    
    if ([result objectForKey:@"photos"]==nil) {
        NSError*  error = [NSError errorWithDomain:AMLocalizedString(@"SERVER_ERROR", nil) code:0
                                          userInfo:nil];
        
        
        [self.delegate serverConnection:self ISFailedByReason:error];
        return;
        
    }else{
        [self.delegate serverConnection:self ISFinishSuccessWithData:result];

    }

    
}



+(int) checkInternetConnection {
    
    reach = [Reachability reachabilityForInternetConnection];
    // reach = [Reachability reachabilityWithHostname:WEB_SERVICE_URL];
    NetworkStatus netStatus = [reach currentReachabilityStatus];
    
    reach.reachableBlock = ^(Reachability * reachability)
    {
        
        //connection good  >> check for update
        
        
    };
    
    
    reach.unreachableBlock = ^(Reachability * reachability)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            switch (netStatus)
            {
                    
                case ReachableViaWWAN:
                {
                    
                    break;
                }
                case ReachableViaWiFi:
                {
                    
                    break;
                }
                case NotReachable:
                {
                    
                    
                    break;
                }
            }
            
            
        });
    };
    
    
    return netStatus;
    
}

+(void)startNotify{
    [reach startNotifier];
    
}
#pragma mark - NSURLConnectionDelegate -

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [receivedData appendData:data];
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    [self jsonDataLoad:receivedData];
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [self.delegate serverConnection:self ISFailedByReason:error];
}

@end

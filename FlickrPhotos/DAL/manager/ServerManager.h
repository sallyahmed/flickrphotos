//
//  ServerManager.h
//  AlAmoudi
//
//  Created by Tech World Company on 9/2/15.
//  Copyright (c) 2015 SallyZone. All rights reserved.
//
#import "ServerManager.h"
#import <Foundation/Foundation.h>

@protocol ServerManagerDelegate <NSObject>
-(void)serverConnection:(id)connection ISFinishSuccessWithData:(id) data;
-(void)serverConnection:(id)connection ISFailedByReason:(NSError *)error;
@end

#import "Reachability.h"

@interface ServerManager : NSObject<NSURLConnectionDelegate,NSURLConnectionDataDelegate>
@property (nonatomic,weak)id<ServerManagerDelegate> delegate;
-(id)initWithDelegate:(id <ServerManagerDelegate>)delegate;
+(int) checkInternetConnection ;
+(void)startNotify;

-(void)webConnectionWithFunction:(NSString *) method;

@end

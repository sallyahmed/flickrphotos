//
//  FlickrPhotoModel.h
//  FlickrPhotos
//
//  Created by Sally on 5/24/16.
//  Copyright © 2016 Sally Ahmed. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GeneralProtocol.h"

@interface FlickrPhotoModel : NSObject<GeneralProtocol>
+ (id)getInstance;
-(void)setObject:(NSMutableArray *)obj;
-(NSMutableArray *)getAll;
-(int)getCurrentIndex;
-(int)getTotalPages;
-(void)reset;


-(void)setUserObject:(NSMutableArray *)obj;
-(NSMutableArray *)getAllUserPhotos;
-(int)getCurrentIndexForUserPhotos;
-(int)getTotalPagesForUserPhotos;
-(void)resetForUserPhotos;
@end

//
//  FlickrPhotoModel.m
//  FlickrPhotos
//
//  Created by Sally on 5/24/16.
//  Copyright © 2016 Sally Ahmed. All rights reserved.
//

#import "FlickrPhotoModel.h"
#import "FlickrPhoto.h"
@implementation FlickrPhotoModel
static NSMutableArray *allObjects = nil;
static int totalPages = 1;
static int currentPage = 1;


static NSMutableArray *allUserObjects = nil;
static int totalUserPages = 1;
static int currentUserPage = 1;

+ (id)getInstance {
    static FlickrPhotoModel *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

-(void)setObject:(id)obj{
    @try {
        currentPage ++;
        if (currentPage == 2) {
        
            allObjects = [NSMutableArray array];

        }
        totalPages = [[[obj objectForKey:@"photos"] objectForKey:@"total"] intValue];
        
        for (NSMutableDictionary * dic  in [[obj objectForKey:@"photos"] objectForKey:@"photo"]) {
            
            FlickrPhoto * item = [[FlickrPhoto alloc]initWithDictionary:dic];
            [allObjects addObject:item];
        }
    }
    @catch (NSException *exception) {
        
    }
    
}

-(void)reset{
    currentPage = 1;
    allObjects = [NSMutableArray array];
}
-(NSMutableArray *)getAll{
    
    return allObjects;
}

-(int)getCurrentIndex{
    return currentPage;
}

-(int)getTotalPages{
    return totalPages;
}





-(void)setUserObject:(id)obj{
    @try {
        currentUserPage ++;
        if (currentUserPage == 2) {
            
            allUserObjects = [NSMutableArray array];
            
        }
        totalUserPages = [[[obj objectForKey:@"photos"] objectForKey:@"total"] intValue];
        
        for (NSMutableDictionary * dic  in [[obj objectForKey:@"photos"] objectForKey:@"photo"]) {
            
            FlickrPhoto * item = [[FlickrPhoto alloc]initWithDictionary:dic];
            [allUserObjects addObject:item];
        }
    }
    @catch (NSException *exception) {
        
    }

}
-(NSMutableArray *)getAllUserPhotos{
    return allUserObjects;
}
-(int)getCurrentIndexForUserPhotos{
    return currentUserPage;
}
-(int)getTotalPagesForUserPhotos{
    return totalUserPages;
}
-(void)resetForUserPhotos{
    currentUserPage = 1;
    allUserObjects = [NSMutableArray array];
}
@end

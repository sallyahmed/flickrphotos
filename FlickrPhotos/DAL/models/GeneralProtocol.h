//
//  GeneralProtocol.h
//  AlAmoudi
//
//  Created by Tech World Company on 9/7/15.
//  Copyright (c) 2015 SallyZone. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GeneralProtocol <NSObject>
+ (id)getInstance;
@optional
-(void)setObject:(id)obj;
-(id)getAll;
-(id)getByID:(int) ID;
@end

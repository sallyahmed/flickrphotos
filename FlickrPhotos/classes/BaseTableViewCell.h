//
//  BaseTableViewCell.h
//  AlAmoudi
//
//  Created by Tech World Company on 9/6/15.
//  Copyright (c) 2015 SallyZone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *bgV;

@end

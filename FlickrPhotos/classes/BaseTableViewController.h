//
//  BaseTableViewController.h
//  FlickrPhotos
//
//  Created by Sally on 5/24/16.
//  Copyright © 2016 Sally Ahmed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseTableViewController : UITableViewController
-(void)showLoader;
-(void)hideLoader;
-(void)showAlertWith :(NSString *)message;
@end

//
//  ApplicationConstant.h
//  AlAmoudi
//
//  Created by Tech World Company on 11/17/1436 AH.
//  Copyright (c) 1436 SallyZone. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ApplicationConstant : NSObject



#define BASE_URL @"https://api.flickr.com/services/rest/?method="

#define NETWORK_TIMEOUT 5
#define FlickrAPIKey @"2f852b9c9b79a4ab5af440f3e56be1cb"


#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


#if TARGET_IPHONE_SIMULATOR
#define NSLog(fmt,...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#define NSLog(...)
#endif
@end

//
//  BaseTableViewController.m
//  FlickrPhotos
//
//  Created by Sally on 5/24/16.
//  Copyright © 2016 Sally Ahmed. All rights reserved.
//

#import "BaseTableViewController.h"
#import "KVNProgress.h"

@interface BaseTableViewController ()

@end

@implementation BaseTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)showLoader{
    [KVNProgress show];
}
-(void)hideLoader{
    [KVNProgress dismiss];
}

-(void)showAlertWith :(NSString *)message{
    @try {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"" message:message delegate:nil cancelButtonTitle:AMLocalizedString(@"OK", nil) otherButtonTitles: nil];
        [alert show];
    }
    @catch (NSException *exception) {
    }
}

@end

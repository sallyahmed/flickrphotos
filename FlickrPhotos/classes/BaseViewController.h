//
//  BaseViewController.h
//  FlickrPhotos
//
//  Created by Sally on 5/23/16.
//  Copyright © 2016 Sally Ahmed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController
-(void)showLoader;
-(void)hideLoader;
-(void)showAlertWith :(NSString *)message;
@end

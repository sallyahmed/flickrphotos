//
//  UserFlickrImageViewController.h
//  FlickrPhotos
//
//  Created by Sally on 5/24/16.
//  Copyright © 2016 Sally Ahmed. All rights reserved.
//

#import "BaseViewController.h"
#import "BaseTableViewController.h"
@interface UserFlickrImageViewController : BaseTableViewController<ServerManagerDelegate>
@property (nonatomic , strong) NSString * owner;
@end

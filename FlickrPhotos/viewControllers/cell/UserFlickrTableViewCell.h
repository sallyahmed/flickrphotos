//
//  UserFlickrTableViewCell.h
//  FlickrPhotos
//
//  Created by Sally on 5/24/16.
//  Copyright © 2016 Sally Ahmed. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface UserFlickrTableViewCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView;

@property (weak, nonatomic) IBOutlet UILabel *titleLbl;

@end

//
//  FlickrPhotoTableViewCell.m
//  FlickrPhotos
//
//  Created by Sally on 5/23/16.
//  Copyright © 2016 Sally Ahmed. All rights reserved.
//

#import "FlickrPhotoTableViewCell.h"

@implementation FlickrPhotoTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

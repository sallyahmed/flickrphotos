//
//  FlickrPhotoTableViewCell.h
//  FlickrPhotos
//
//  Created by Sally on 5/23/16.
//  Copyright © 2016 Sally Ahmed. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FlickrPhotoTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (strong, nonatomic)  NSString *owner;

@end

//
//  UserFlickrImageViewController.m
//  FlickrPhotos
//
//  Created by Sally on 5/24/16.
//  Copyright © 2016 Sally Ahmed. All rights reserved.
//

#import "UserFlickrImageViewController.h"
#import "FlickrPhotoModel.h"
#import "NSString+General.h"
#import "UserFlickrTableViewCell.h"
@interface UserFlickrImageViewController ()

@end

@implementation UserFlickrImageViewController
ServerManager * userConn;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self showLoader];
    userConn = [[ServerManager alloc]initWithDelegate:self];
    [userConn webConnectionWithFunction:[NSString stringWithFormat:@"flickr.photos.search&api_key=%@&per_page=30&format=json&nojsoncallback=1&page=%ld&user_id=%@",FlickrAPIKey,(long)[[FlickrPhotoModel getInstance] getCurrentIndexForUserPhotos],self.owner]];

}
#pragma mark - UITableViewControllerDelegate


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [[[FlickrPhotoModel getInstance] getAllUserPhotos] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UserFlickrTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"userCell"];
    cell.titleLbl.text = ((FlickrPhoto *)[[[FlickrPhotoModel getInstance] getAllUserPhotos] objectAtIndex:indexPath.row]).title ;
    [cell.imgView sd_setImageWithURL:[NSURL URLWithString:((FlickrPhoto *)[[[FlickrPhotoModel getInstance] getAllUserPhotos] objectAtIndex:indexPath.row]).imgURL ]
                    placeholderImage:[UIImage imageNamed:@"placeholder.png"]
                             options:SDWebImageRefreshCached];
    
    if (indexPath.row == [[[FlickrPhotoModel getInstance] getAllUserPhotos] count] -1 ){
        [self checkForAppend];
    }
    return cell;
    
}
-(void)checkForAppend{
    if([[FlickrPhotoModel getInstance] getTotalPagesForUserPhotos]==[[[FlickrPhotoModel getInstance] getAllUserPhotos] count]){
        
        return;
    }
    
    else{
        
        userConn = [[ServerManager alloc]initWithDelegate:self];
        [userConn webConnectionWithFunction:[NSString stringWithFormat:@"flickr.photos.search&api_key=%@&per_page=30&format=json&nojsoncallback=1&page=%ld&user_id=%@",FlickrAPIKey,(long)[[FlickrPhotoModel getInstance] getCurrentIndexForUserPhotos],self.owner]];
    }
    
}



#pragma mark - ServerDelegte


-(void)serverConnection:(id)connection ISFinishSuccessWithData:(id) data{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self hideLoader];
        
        [[FlickrPhotoModel getInstance]setUserObject:data];
        
        [self.tableView reloadData];
    });
    
}
-(void)serverConnection:(id)connection ISFailedByReason:(NSError *)error{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self hideLoader];
        
        [self showAlertWith:error.localizedDescription];
        
    });
}

@end

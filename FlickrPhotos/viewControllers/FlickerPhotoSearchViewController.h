//
//  FlickerPhotoSearchViewController.h
//  FlickrPhotos
//
//  Created by Sally on 5/23/16.
//  Copyright © 2016 Sally Ahmed. All rights reserved.
//

#import "BaseViewController.h"

@interface FlickerPhotoSearchViewController : BaseViewController<UITableViewDataSource , UITableViewDelegate ,UISearchBarDelegate,ServerManagerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@end

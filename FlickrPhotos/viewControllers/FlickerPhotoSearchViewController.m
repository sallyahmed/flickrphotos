//
//  FlickerPhotoSearchViewController.m
//  FlickrPhotos
//
//  Created by Sally on 5/23/16.
//  Copyright © 2016 Sally Ahmed. All rights reserved.
//

#import "FlickerPhotoSearchViewController.h"
#import "FlickrPhotoTableViewCell.h"
#import "FlickrPhotoModel.h"
#import "NSString+General.h"
#import "UserFlickrImageViewController.h"
@interface FlickerPhotoSearchViewController ()

@end

@implementation FlickerPhotoSearchViewController
ServerManager * photoConn;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - UITableViewControllerDelegate


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [[[FlickrPhotoModel getInstance] getAll] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    FlickrPhotoTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"photoCell"];
    cell.titleLbl.text = ((FlickrPhoto *)[[[FlickrPhotoModel getInstance] getAll] objectAtIndex:indexPath.row]).title ;
    [cell.imgView sd_setImageWithURL:[NSURL URLWithString:((FlickrPhoto *)[[[FlickrPhotoModel getInstance] getAll] objectAtIndex:indexPath.row]).imgURL ]
                 placeholderImage:[UIImage imageNamed:@"placeholder.png"]
                          options:SDWebImageRefreshCached];
    
    cell.owner = ((FlickrPhoto *)[[[FlickrPhotoModel getInstance] getAll] objectAtIndex:indexPath.row]).owner ;
    if (indexPath.row == [[[FlickrPhotoModel getInstance] getAll] count] -1 ){
        [self checkForAppend];
    }
    return cell;

}


-(void)checkForAppend{
    if([[FlickrPhotoModel getInstance] getTotalPages]==[[[FlickrPhotoModel getInstance] getAll] count]){
        
        return;
    }
    
    else{
        
        photoConn = [[ServerManager alloc]initWithDelegate:self];
        [photoConn webConnectionWithFunction:[NSString stringWithFormat:@"flickr.photos.search&api_key=%@&tags=%@&per_page=30&format=json&nojsoncallback=1&page=%ld",FlickrAPIKey,[_searchBar.text urlencode],(long)[[FlickrPhotoModel getInstance] getCurrentIndex]]];
    }
    
}
#pragma mark - SearchDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar1{
    [self.view endEditing:YES];
    [[FlickrPhotoModel getInstance]reset];
    if (![searchBar1.text isEmpty]) {
        [self showLoader];
        
        photoConn = [[ServerManager alloc]initWithDelegate:self];
        [photoConn webConnectionWithFunction:[NSString stringWithFormat:@"flickr.photos.search&api_key=%@&tags=%@&per_page=30&format=json&nojsoncallback=1&page=%ld",FlickrAPIKey,[searchBar1.text urlencode],(long)[[FlickrPhotoModel getInstance] getCurrentIndex]]];
        
    }
}


#pragma mark - ServerDelegte


-(void)serverConnection:(id)connection ISFinishSuccessWithData:(id) data{
    dispatch_async(dispatch_get_main_queue(), ^{
    [self hideLoader];
    
    [[FlickrPhotoModel getInstance]setObject:data];
        
        [self.tableView reloadData];
    });
    
}
-(void)serverConnection:(id)connection ISFailedByReason:(NSError *)error{
    dispatch_async(dispatch_get_main_queue(), ^{

    [self hideLoader];
        [self showAlertWith:error.localizedDescription];

    });
}

#pragma mark - prepareForSegue


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ShowUserPhotos"])
    {
        UserFlickrImageViewController * vc = segue.destinationViewController;
        vc.owner = ((FlickrPhotoTableViewCell *)sender).owner;
    }
}
@end
